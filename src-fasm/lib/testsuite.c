#include <stdio.h>
#include <assert.h>
extern int lib_strlen(const char *s);

int main(void)
{
	assert(lib_strlen("") == 0);
	assert(lib_strlen("foo") == 3);

	return 0;
}
