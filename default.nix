{ nixpkgs ?
  (import <nixpkgs> {}).fetchFromGitHub {
    owner = "NixOS"; # commit of 2019-02-25
    repo = "nixpkgs";
    rev = "10f0d40838cd4160ae7c7ceaaba48b0532c89887";
    sha256 = "0gcd1iqjn2y40hhgnc2p2i7g881xlgrpng5n6gs1qxcymxgkgfz4";
  }
}:
let pkgs = import nixpkgs {};
    cdb  = pkgs.callPackage ./nix/cdb.nix {};
in pkgs.stdenv.mkDerivation rec {
  name = "unix-spirit-${version}";
  version = "0.1";
  src = builtins.fetchGit { url = ./.; ref = "HEAD"; };
  buildInputs = with pkgs;
    [ tup fasm gdbm cdb gcc ];
  buildPhase = ''
    tup init
    tup generate build.sh
    sh -eu build.sh
  '';
  installPhase = ''
    mkdir -p $out/bin
    cp freezedb $out/bin
  '';
}

