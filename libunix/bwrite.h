#ifndef BWRITE_BUFFER_SIZE
#define BWRITE_BUFFER_SIZE 1024
#endif

struct bwrite {
	int fd;
	char buffer[BWRITE_BUFFER_SIZE];
	size_t occupied;
};

void bwrite_init(struct bwrite *b, int fd);
int bwrite_put(struct bwrite *b, void *data, size_t len);
int bwrite_putc(struct bwrite *b, char c);
int bwrite_flush(struct bwrite *b);
