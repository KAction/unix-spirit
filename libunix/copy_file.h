/*
 * Use linux-specific system call; faster than naive read-write look
 * and faster, than cp(1) from coreutils
 */
int copy_file(int in, int out);
