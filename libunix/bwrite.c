#include <string.h>
#include <unistd.h>

#include "bwrite.h"

enum { buffer_size = BWRITE_BUFFER_SIZE };

int
bwrite_flush(struct bwrite *b)
{
	size_t written = 0;

	while (written != b->occupied) {
		size_t bytes = write(b->fd, b->buffer, b->occupied - written);
		if (bytes == -1)
			return 1;
		written += bytes;
	}
	b->occupied = 0;
	return 0;
}

/* For now, assuming that len < BWRITE_BUFFER_SIZE */
int
bwrite_put(struct bwrite *b, void *data, size_t len)
{
	if (len + b->occupied > buffer_size) {
		int err = bwrite_flush(b);
		if (err)
			return err;
	}
	memcpy(b->buffer + b->occupied, data, len);
	b->occupied += len;
	return 0;
}

int 
bwrite_putc(struct bwrite *b, char c)
{
	return bwrite_put(b, &c, 1);
}

void
bwrite_init(struct bwrite *b, int fd)
{
	b->fd = fd;
	b->occupied = 0;
}

