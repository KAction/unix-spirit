#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

/* Return size of file, associated with file descriptor {fd}. */
static off_t fsize(int fd)
{
	struct stat sb;
	int err;

	err = fstat(fd, &sb);
	if (err) {
		return -1;
	}
	return sb.st_size;
}

int
copy_file(int in, int out)
{
	loff_t off_in, off_out;
	off_t len;

	len = fsize(in);

	if (len == 0) {
		errno = EINVAL;
		return -1;
	} else if (len == -1) {
		return -1;
	}
	
	off_in = off_out = 0;
	

	do {
		loff_t ret;
		ret = syscall(__NR_copy_file_range,
	                      in, &off_in,
	                      out, &off_out,
	                      len, 0);
		if (ret == -1)
			return -1;
		len -= ret;
	} while (len > 0);

	return 0;
}
