Collection of small programs
============================

This repository contains small programs, implemented in C, that
in my opinion, follow spirit of Unix -- do one thing, do it well,
delegate to subprocessed as much, as possible.

